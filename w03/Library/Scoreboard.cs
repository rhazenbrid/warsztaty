﻿namespace Warsztaty;

public class Scoreboard
{
    public static void Clear(string[,] scoreboard)
    {
        for (int i = 0; i < scoreboard.GetLength(0); ++i)
        {
            scoreboard[i, 0] = "---";
            scoreboard[i, 1] = "0";
        }
    }

    public static void Display(string[,] scoreboard)
    {
        for (int i = 0; i < scoreboard.GetLength(0); ++i)
        {
            Console.WriteLine($"#{i+1} {scoreboard[i, 0]} {scoreboard[i, 1]}");
        }
    }

    public static void Insert(string[,] scoreboard, string playerName, string score)
    {
        int s = int.Parse(score);

        for (int i = 0; i < scoreboard.GetLength(0); ++i)
        {
            if (s > int.Parse(scoreboard[i, 1]))
            {
                for (int k = scoreboard.GetLength(0) - 1; k > i; --k)
                {
                    scoreboard[k, 0] = scoreboard[k - 1, 0];
                    scoreboard[k, 1] = scoreboard[k - 1, 1];
                }

                scoreboard[i, 0] = playerName;
                scoreboard[i, 1] = score;
                break;
            }
        }
    }
}
