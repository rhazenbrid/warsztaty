using NUnit.Framework;

namespace  Warsztaty.Tests;

public class ScoreboardTests
{
    private string[,] scoreboard;

    [SetUp]
    public void Setup()
    {
        scoreboard = new string[5, 2];
        Scoreboard.Clear(scoreboard);
    }

    [Test]
    public void HighScoreboard_Empty()
    {
        string[,] expected = new string[,]
        {
            {"---", "0"},
            {"---", "0"},
            {"---", "0"},
            {"---", "0"},
            {"---", "0"},
        };

        CollectionAssert.AreEqual(expected, scoreboard);
    }

    [Test]
    public void HighScoreboard_InsertRecord_ToLow()
    {
        scoreboard[0, 0] = "Bartek";
        scoreboard[0, 1] = "200";
        scoreboard[1, 0] = "Dariusz";
        scoreboard[1, 1] = "175";
        scoreboard[2, 0] = "Cecylia";
        scoreboard[2, 1] = "150";
        scoreboard[3, 0] = "Ewa";
        scoreboard[3, 1] = "125";
        scoreboard[4, 0] = "Anna";
        scoreboard[4, 1] = "100";

        Scoreboard.Insert(scoreboard, "Felix", "50");

        string[,] expected = new string[,]
        {
            {"Bartek", "200"},
            {"Dariusz", "175"},
            {"Cecylia", "150"},
            {"Ewa", "125"},
            {"Anna", "100"},
        };

        CollectionAssert.AreEqual(expected, scoreboard);
    }

    [Test]
    public void HighScoreboard_InsertRecord_Middle()
    {
        scoreboard[0, 0] = "Cecylia";
        scoreboard[0, 1] = "150";
        scoreboard[1, 0] = "Anna";
        scoreboard[1, 1] = "100";

        Scoreboard.Insert(scoreboard, "Ewa", "125");

        string[,] expected = new string[,]
        {
            {"Cecylia", "150"},
            {"Ewa", "125"},
            {"Anna", "100"},
            {"---", "0"},
            {"---", "0"},
        };

        CollectionAssert.AreEqual(expected, scoreboard);
    }

    [Test]
    public void HighScoreboard_InsertRecord_Top()
    {
        scoreboard[0, 0] = "Cecylia";
        scoreboard[0, 1] = "150";
        scoreboard[1, 0] = "Anna";
        scoreboard[1, 1] = "100";

        Scoreboard.Insert(scoreboard, "Bartek", "200");

        string[,] expected = new string[,]
        {
            {"Bartek", "200"},
            {"Cecylia", "150"},
            {"Anna", "100"},
            {"---", "0"},
            {"---", "0"},
        };

        CollectionAssert.AreEqual(expected, scoreboard);
    }

    [Test]
    public void HighScoreboard_InsertRecord_Bottom()
    {
        scoreboard[0, 0] = "Cecylia";
        scoreboard[0, 1] = "150";
        scoreboard[1, 0] = "Anna";
        scoreboard[1, 1] = "100";

        Scoreboard.Insert(scoreboard, "Felix", "50");

        string[,] expected = new string[,]
        {
            {"Cecylia", "150"},
            {"Anna", "100"},
            {"Felix", "50"},
            {"---", "0"},
            {"---", "0"},
        };

        CollectionAssert.AreEqual(expected, scoreboard);
    }
}
