﻿using Warsztaty;

string[,] scoreboard = new string[5, 2];

Scoreboard.Clear(scoreboard);
Scoreboard.Display(scoreboard);

string[] input = new string[2];

Console.WriteLine("Podaj wynik:");
input = Console.ReadLine()?.Split(" ");
Scoreboard.Insert(scoreboard, input[0], input[1]);
Scoreboard.Display(scoreboard);

Console.WriteLine("Podaj wynik:");
input = Console.ReadLine()?.Split(" ");
Scoreboard.Insert(scoreboard, input[0], input[1]);
Scoreboard.Display(scoreboard);

Console.WriteLine("Podaj wynik:");
input = Console.ReadLine()?.Split(" ");
Scoreboard.Insert(scoreboard, input[0], input[1]);
Scoreboard.Display(scoreboard);
