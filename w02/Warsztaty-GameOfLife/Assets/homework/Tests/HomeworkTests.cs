using NUnit.Framework;

public class HomeworkTests
{
    [Test]
    public void AreEqual_CompareSameTables()
    {
        int[] a = {1, 1, 3, 7};
        int[] b = {1, 1, 3, 7};

        bool result = Homework.AreEqual(a, b);

        Assert.IsTrue(result);
    }

    [Test]
    public void AreEqual_IncorrectOrder()
    {
        int[] a = {1, 1, 3, 7};
        int[] b = {7, 3, 1, 1};

        bool result = Homework.AreEqual(a, b);

        Assert.IsFalse(result);
    }

    [Test]
    public void AreEqual_DifferentLength()
    {
        int[] a = {1, 1, 3, 7};
        int[] b = {1, 1, 3, 7, 15};

        bool result = Homework.AreEqual(a, b);

        Assert.IsFalse(result);
    }

    [Test]
    public void AreEqual_DontThrow()
    {
        Assert.DoesNotThrow(() =>
        {
            Homework.AreEqual(null, null);
        });
    }

    [TestCase(new [] {1}, new [] {3, 2, 1})]
    [TestCase(new [] {1, 2}, new [] {0, 1, 2, 3})]
    [TestCase(new [] {1, 2, 2}, new [] {1, 2, 1, 2, 2})]
    public void Contains_True(int[] a, int[] b)
    {
        Assert.IsTrue(Homework.Contains(a, b));
    }

    [TestCase(new int[0], new int[0])]
    [TestCase(new [] {1}, new int[0])]
    [TestCase(new [] {2, 4}, new [] {1, 3})]
    [TestCase(new [] {1, 2, 2}, new [] {1, 2, 1, 2, 1})]
    public void Contains_False(int[] a, int[] b)
    {
        Assert.IsFalse(Homework.Contains(a, b));
    }

    [TestCase(null, null)]
    public void Contains_DontThrow(int[] a, int[] b)
    {
        Assert.DoesNotThrow(() => {
            Homework.Contains(a, b);
        });
    }

    [TestCase(new [] {0, 1, 2, 3}, 0, 2, 2, new [] {2, 3, 2, 3})]
    [TestCase(new [] {0, 1, 2, 3}, 2, 0, 2, new [] {0, 1, 0, 1})]
    public void Copy_NoOverlap(int[] array, int destination, int source, int length, int[] expected)
    {
        Homework.Copy(array, destination, source, length);

        Assert.AreEqual(expected, array);
    }

    [TestCase(new [] {0, 1, 2, 3}, 0, 1, 2, new [] {1, 2, 2, 3})]
    [TestCase(new [] {0, 1, 2, 3}, 1, 1, 2, new [] {0, 1, 2, 3})]
    [TestCase(new [] {0, 1, 2, 3}, 2, 0, 2, new [] {0, 1, 0, 1})]
    public void Copy_Overlap(int[] array, int destination, int source, int length, int[] expected)
    {
        Homework.Copy(array, destination, source, length);

        Assert.AreEqual(expected, array);
    }

    [TestCase(new [] {0, 1, 2, 3}, -1, 1, 2, new [] {2, 1, 2, 3})]
    [TestCase(new [] {0, 1, 2, 3}, -1, 1, 3, new [] {2, 3, 2, 3})]
    [TestCase(new [] {0, 1, 2, 3}, -1, 2, 3, new [] {3, 1, 2, 3})]
    [TestCase(new [] {0, 1, 2, 3}, -1, 3, 1, new [] {0, 1, 2, 3})]
    [TestCase(new [] {0, 1, 2, 3}, 0, 3, 2, new [] {3, 1, 2, 3})]
    [TestCase(new [] {0, 1, 2, 3}, 0, 2, 3, new [] {2, 3, 2, 3})]  
    [TestCase(new [] {0, 1, 2, 3}, 2, 0, 9, new [] {0, 1, 0, 1})]
    [TestCase(new [] {0, 1, 2, 3}, 2, -1, 2, new [] {0, 1, 2, 0})]
    [TestCase(new [] {0, 1, 2, 3}, 2, -1, 4, new [] {0, 1, 2, 0})]
    [TestCase(new [] {0, 1, 2, 3}, 4, -1, 1, new [] {0, 1, 2, 3})]
    [TestCase(new [] {0, 1, 2, 3}, 4, 0, 1, new [] {0, 1, 2, 3})]
    public void Copy_Bounds(int[] array, int destination, int source, int length, int[] expected)
    {
        Homework.Copy(array, destination, source, length);

        Assert.AreEqual(expected, array);
    }

    [TestCase(null, 1, 0, 1)]
    [TestCase(new int[0], 1, 0, 1)]
    [TestCase(new [] {0, 1, 2, 3}, 0, 2, -1)]
    public void Copy_DontThrow(int[] array, int destination, int source, int length)
    {
        Assert.DoesNotThrow(() => {
            Homework.Copy(array, destination, source, length);
        });
    }
}
