using UnityEngine;

public class Game : MonoBehaviour
{
    private bool[,] cells;

    private bool autoPlay;
    private TilesBoard view;
    private Color aliveCellColor = Color.yellow;
    private Color deadCellColor = new Color(0.341f, 0.549f, 0.709f);

    private Color backgroundColorPlaying = Color.black;
    private Color backgroundColorNotPlaying = new Color(0.215f, 0.215f, 0.215f);

    private float duration = 0f;
    private float threshold = 0.5f;

    private void Start()
    {
        int width = 12;
        int height = 9;

        cells = new bool[width, height];

        view = TilesBoard.Create(width, height, deadCellColor, OnClick);

        SetColors();

        view.SetBackground(autoPlay ? backgroundColorPlaying : backgroundColorNotPlaying);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            autoPlay = !autoPlay;
            duration = 0f;

            view.SetBackground(autoPlay ? backgroundColorPlaying : backgroundColorNotPlaying);
        }

        if (autoPlay)
        {
            duration += Time.deltaTime;

            if (duration > threshold)
            {
                duration = 0f;
                NextStep();
                SetColors();
            }
        }
    }

    private void OnClick(int x, int y)
    {
        cells[x, y] = !cells[x, y];
        SetColor(x, y);
    }

    public Color GetCellColor(bool alive)
    {
        if (alive)
        {
            return aliveCellColor;
        }
        else
        {
            return deadCellColor;
        }
    }

    private void SetColor(int x, int y)
    {
        Color c = GetCellColor(cells[x, y]);
        view.SetColor(x, y, c);
    }

    private void SetColors()
    {
        for (int x = 0; x < cells.GetLength(0); ++x)
        {
            for (int y = 0; y < cells.GetLength(1); ++y)
            {
                SetColor(x, y);
            }
        }
    }

    private void NextStep()
    {
        bool[,] tmp = new bool[cells.GetLength(0), cells.GetLength(1)];

        for (int x = 0; x < cells.GetLength(0); ++x)
        {
            for (int y = 0; y < cells.GetLength(1); ++y)
            {
                tmp[x, y] = IsCellAlive(x, y);
            }
        }

        cells = tmp;
    }

    private bool IsCellAlive(int x, int y)
    {
        int n = 0;

        for (int b = y - 1; b <= y + 1; b += 1)
        {
            for (int a = x - 1; a <= x + 1; a += 1)
            {
                if (a == x && b == y)
                    continue;

                if (a < 0 || a >= cells.GetLength(0))
                    continue;
                
                if (b < 0 || b >= cells.GetLength(1))
                    continue;

                if (cells[a, b])
                {
                    n += 1;
                }
            }
        }

        if (cells[x, y])
        {
            if (n == 2 || n == 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (n == 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
