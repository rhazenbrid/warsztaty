public class GameModel
{
    public int Width{ get; private set; }
    public int Height { get; private set; }
    public bool[,] Cells { get; private set; }


    public GameModel(int width, int height)
    {
        Width = width;
        Height = height;

        Cells = new bool[width, height];
    }

    public void NextStep()
    {
        bool[,] tmp = new bool[Width, Height];

        for (int x = 0; x < Width; ++x)
        {
            for (int y = 0; y < Height; ++y)
            {
                tmp[x, y] = IsCellAlive(x, y);
            }
        }

        Cells = tmp;
    }

    private bool IsCellAlive(int x, int y)
    {
        int n = 0;

        for (int b = y - 1; b <= y + 1; b += 1)
        {
            for (int a = x - 1; a <= x + 1; a += 1)
            {
                if (a == x && b == y)
                    continue;

                if (a < 0 || a >= Width)
                    continue;
                
                if (b < 0 || b >= Height)
                    continue;

                if (Cells[a, b])
                {
                    n += 1;
                }
            }
        }

        if (Cells[x, y])
        {
            if (n == 2 || n == 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if (n == 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
