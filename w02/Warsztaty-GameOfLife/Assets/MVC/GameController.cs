using UnityEngine;

public class GameController : MonoBehaviour
{
    private GameModel model;

    private bool autoPlay;
    private TilesBoard view;
    private Color aliveCellColor = Color.yellow;
    private Color deadCellColor = new Color(0.341f, 0.549f, 0.709f);

    private Color backgroundColorPlaying = Color.black;
    private Color backgroundColorNotPlaying = new Color(0.215f, 0.215f, 0.215f);

    private float duration = 0f;
    private float threshold = 0.5f;

    private void Start()
    {
        model = new GameModel(12, 9);

        view = TilesBoard.Create(model.Width, model.Height, deadCellColor, OnClick);

        SetColors();

        view.SetBackground(autoPlay ? backgroundColorPlaying : backgroundColorNotPlaying);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            autoPlay = !autoPlay;
            duration = 0f;

            view.SetBackground(autoPlay ? backgroundColorPlaying : backgroundColorNotPlaying);
        }

        if (autoPlay)
        {
            duration += Time.deltaTime;

            if (duration > threshold)
            {
                duration = 0f;
                model.NextStep();
                SetColors();
            }
        }
    }

    // metoda
    private void OnClick(int x, int y)
    {
        model.Cells[x, y] = !model.Cells[x, y];
        SetColor(x, y);
    }

    public Color GetCellColor(bool alive)
    {
        if (alive)
        {
            return aliveCellColor;
        }
        else
        {
            return deadCellColor;
        }
    }

    private void SetColor(int x, int y)
    {
        Color c = GetCellColor(model.Cells[x, y]);
        view.SetColor(x, y, c);
    }

    private void SetColors()
    {
        for (int x = 0; x < model.Width; ++x)
        {
            for (int y = 0; y < model.Height; ++y)
            {
                SetColor(x, y);
            }
        }
    }
}
