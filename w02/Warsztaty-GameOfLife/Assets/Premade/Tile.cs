﻿using System;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public event Action Clicked;
    
    public void OnMouseDown()
    {
        Clicked?.Invoke();
    }
}
