﻿using System;
using UnityEngine;

public class TilesBoard : MonoBehaviour
{
    public static TilesBoard Create(int width, int height, Color fillColor, Action<int, int> clickAction)
    {
        var gameObject = new GameObject("GameView");
        var view = gameObject.AddComponent<TilesBoard>();
        view.Initialize(width, height, fillColor, clickAction);
        return view;
    }

    private Tile[,] tiles;

    public void Initialize(int width, int height, Color fillColor, Action<int, int> clickAction)
    {
        float size = Mathf.Min(18f / width, 10f / height);
        float scale = 0.95f * size;

        var tilePrefab = Resources.Load<Tile>("Tile");
        tiles = new Tile[width, height];
        for (int w = 0; w < width; ++w)
        {
            for (int h = 0; h < height; ++h)
            {
                Tile tile = Instantiate(tilePrefab, transform, true);
                Transform tileTransform = tile.transform;
                float x = w - (width - 1) / 2f;
                float y = h - (height - 1) / 2f;
                tileTransform.localPosition = new Vector3(x * size, y * size);
                tileTransform.localScale = new Vector3(scale, scale, 1f);
                tile.name = $"({w}, {h})";
                int w1 = w;
                int h1 = h;
                tile.Clicked += () => clickAction(w1, h1);
                tiles[w, h] = tile;
            }
        }

        FillColor(fillColor);
    }

    public void SetColor(int x, int y, Color color)
    {
        tiles[x, y].spriteRenderer.color = color;
    }

    public void FillColor(Color color)
    {
        for (int x = 0; x < tiles.GetLength(0); ++x)
        {
            for (int y = 0; y < tiles.GetLength(1); ++y)
            {
                tiles[x, y].spriteRenderer.color = color;
            }
        }
    }

    public void SetBackground(Color color)
    {
        Camera.main.backgroundColor = color;
    }
}
